export * from './material-design-icon.component';
export declare class MaterialDesignIconModule {
    static forRoot(): {
        ngModule: typeof MaterialDesignIconModule;
    };
}
