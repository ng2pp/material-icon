import { Component, Input, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

var MaterialDesignIconComponent = (function () {
    function MaterialDesignIconComponent() {
        this.size = 24;
    }
    return MaterialDesignIconComponent;
}());
MaterialDesignIconComponent.decorators = [
    { type: Component, args: [{
                selector: 'mdi',
                template: "<i class=\"material-icons\" [style.font-size]=\"size + 'px'\" [innerHtml]=\"icon\"></i>",
                host: {
                    '[style.height.px]': 'size',
                    '[style.width.px]': 'size'
                }
            },] },
];
/**
 * @nocollapse
 */
MaterialDesignIconComponent.ctorParameters = function () { return []; };
MaterialDesignIconComponent.propDecorators = {
    'icon': [{ type: Input },],
    'size': [{ type: Input },],
};

var MaterialDesignIconModule = (function () {
    function MaterialDesignIconModule() {
    }
    /**
     * @return {?}
     */
    MaterialDesignIconModule.forRoot = function () {
        return {
            ngModule: MaterialDesignIconModule,
        };
    };
    return MaterialDesignIconModule;
}());
MaterialDesignIconModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [
                    MaterialDesignIconComponent,
                ],
                exports: [
                    MaterialDesignIconComponent,
                ]
            },] },
];
/**
 * @nocollapse
 */
MaterialDesignIconModule.ctorParameters = function () { return []; };

export { MaterialDesignIconModule, MaterialDesignIconComponent };
