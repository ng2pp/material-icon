(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common')) :
	typeof define === 'function' && define.amd ? define(['exports', '@angular/core', '@angular/common'], factory) :
	(factory((global['material-icon'] = {}),global._angular_core,global._angular_common));
}(this, (function (exports,_angular_core,_angular_common) { 'use strict';

var MaterialDesignIconComponent = (function () {
    function MaterialDesignIconComponent() {
        this.size = 24;
    }
    return MaterialDesignIconComponent;
}());
MaterialDesignIconComponent.decorators = [
    { type: _angular_core.Component, args: [{
                selector: 'mdi',
                template: "<i class=\"material-icons\" [style.font-size]=\"size + 'px'\" [innerHtml]=\"icon\"></i>",
                host: {
                    '[style.height.px]': 'size',
                    '[style.width.px]': 'size'
                }
            },] },
];
/**
 * @nocollapse
 */
MaterialDesignIconComponent.ctorParameters = function () { return []; };
MaterialDesignIconComponent.propDecorators = {
    'icon': [{ type: _angular_core.Input },],
    'size': [{ type: _angular_core.Input },],
};

var MaterialDesignIconModule = (function () {
    function MaterialDesignIconModule() {
    }
    /**
     * @return {?}
     */
    MaterialDesignIconModule.forRoot = function () {
        return {
            ngModule: MaterialDesignIconModule,
        };
    };
    return MaterialDesignIconModule;
}());
MaterialDesignIconModule.decorators = [
    { type: _angular_core.NgModule, args: [{
                imports: [
                    _angular_common.CommonModule
                ],
                declarations: [
                    MaterialDesignIconComponent,
                ],
                exports: [
                    MaterialDesignIconComponent,
                ]
            },] },
];
/**
 * @nocollapse
 */
MaterialDesignIconModule.ctorParameters = function () { return []; };

exports.MaterialDesignIconModule = MaterialDesignIconModule;
exports.MaterialDesignIconComponent = MaterialDesignIconComponent;

Object.defineProperty(exports, '__esModule', { value: true });

})));
